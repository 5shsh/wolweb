docker_run:
	docker run -d \
      --name=wolweb_test_run \
      -p 8089:8089 \
	  -v $$PWD/.test/devices:/devices \
	  -v $$PWD/.test/config:/config \
	  $(DOCKER_IMAGE):$(DOCKER_TAG)
	docker ps | grep wolweb_test_run

docker_stop:
	docker rm -f wolweb_test_run 2> /dev/null; true